package hu.icell.hr.manager.service;

import hu.icell.hr.manager.model.Employee;

public interface LocalEmployeeService extends RemoteEmployeeService {

	void createEmployee(Employee e);

	void updateEmployee(Employee e);

	void deleteEmployee(Long id);
}
