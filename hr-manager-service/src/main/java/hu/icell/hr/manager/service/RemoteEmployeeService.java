package hu.icell.hr.manager.service;

import java.util.List;

import hu.icell.hr.manager.model.Employee;

public interface RemoteEmployeeService {

	List<Employee> getAllEmployees();
	
	Employee getEmployeeById(Long id);
}
