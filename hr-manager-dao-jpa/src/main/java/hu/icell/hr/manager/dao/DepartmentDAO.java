package hu.icell.hr.manager.dao;

import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Department;

@ApplicationScoped
public class DepartmentDAO extends JpaDAO<Department, Long> {

}
