package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Country;

@ApplicationScoped
public class CountryDAO extends JpaDAO<Country, String> {

}
