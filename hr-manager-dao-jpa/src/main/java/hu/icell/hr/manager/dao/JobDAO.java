package hu.icell.hr.manager.dao;import javax.enterprise.context.ApplicationScoped;

import hu.icell.hr.manager.model.Job;

@ApplicationScoped
public class JobDAO extends JpaDAO<Job, String> {

}
