package hu.icell.hr.manager.service.client;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import hu.icell.hr.manager.model.Employee;
import hu.icell.hr.manager.service.RemoteEmployeeService;

public class EmployeeServiceClient {

	public static void main(String[] args) {

		try {
			Properties props = new Properties();

			props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
			props.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
			props.setProperty("java.naming.factory.state",
					"com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
			props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
			props.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

			InitialContext ic = new InitialContext(props);

			RemoteEmployeeService service = (RemoteEmployeeService) ic
					.lookup("java:global/web-client/EmployeeService!hu.icell.hr.manager.service.RemoteEmployeeService");

			for (Employee e : service.getAllEmployees()) {

				System.out.println("Employee found: " + e);
			}
			
		} catch (NamingException ex) {
			Logger.getLogger(EmployeeServiceClient.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
