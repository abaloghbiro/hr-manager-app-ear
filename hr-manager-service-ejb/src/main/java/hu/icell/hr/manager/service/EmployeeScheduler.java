package hu.icell.hr.manager.service;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

@Singleton
@Startup
public class EmployeeScheduler {

	@Resource
	private TimerService timerService;

	private Logger logger = Logger.getLogger("EmployeeScheduler");

	@PostConstruct
	public void initialize() {

		ScheduleExpression scheduleExpression = new ScheduleExpression().hour("*").minute("*").second("*/15");

		TimerConfig timerConfig = new TimerConfig();
		timerConfig.setInfo("Every 5 second timer");

		timerService.createCalendarTimer(scheduleExpression, timerConfig);
	}

	/*
	 * @Timeout public void checkEmployeeCount(Timer timer) {
	 * 
	 * List<Employee> employees = dao.getAll();
	 * 
	 * logger.info("Timer fired, current employee size : " + employees.size());
	 * logger.info(timer.toString()); }
	 */

	@Schedule(hour = "*", minute = "*", second = "*/5", info = "Every 5 second timer")
	public void checkEmployeeCount5() {

		logger.info("5 sec scheduler....");

	}

	@Schedule(hour = "*", minute = "*", second = "*/7", info = "Every 7 second timer")
	public void checkEmployeeCount10() {
		logger.info("7 sec scheduler....");
	}

}
