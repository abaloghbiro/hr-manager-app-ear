package hu.icell.hr.manager.service;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import hu.icell.hr.manager.dao.EmployeeDAO;
import hu.icell.hr.manager.model.Employee;

@Local(value = { LocalEmployeeService.class })
@Remote(value = { RemoteEmployeeService.class })
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class EmployeeService implements RemoteEmployeeService, LocalEmployeeService {

	@Inject
	private EmployeeDAO dao;

	public List<Employee> getAllEmployees() {
		return dao.getAll();
	}

	public void createEmployee(Employee e) {
		dao.persist(e);
	}

	public void updateEmployee(Employee e) {
		dao.update(e);
	}

	public void deleteEmployee(Long id) {
		dao.delete(getEmployeeById(id));
	}

	public Employee getEmployeeById(Long id) {
		return dao.getById(id);
	}
}
